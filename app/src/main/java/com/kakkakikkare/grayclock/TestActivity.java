package com.kakkakikkare.grayclock;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TestActivity extends Activity {

    private Vibrator myVibrator;

    private long millis = 0;

   @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)){
            long newMillis = System.currentTimeMillis();
            if(millis != 0 && newMillis-millis < 100) {
                try {
                    RunStuff();
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
                millis = 0;
            } else {

                millis = newMillis;
            }
        }
        return true;
    }

    public void StartPressed(View view) throws CameraAccessException {
        RunStuff();
    }

    public void checkboxChecked(View view) {
        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        SharedPreferences.Editor editor = settings.edit();

        editor.putBoolean("checkbox1",((CheckBox)findViewById(R.id.checkBox)).isChecked());
        editor.putBoolean("checkbox2",((CheckBox)findViewById(R.id.checkBox2)).isChecked());
        editor.putBoolean("checkbox3",((CheckBox)findViewById(R.id.checkBox3)).isChecked());

        editor.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        CheckBox checkbox1 = ((CheckBox)findViewById(R.id.checkBox));
        CheckBox checkbox2 = ((CheckBox)findViewById(R.id.checkBox2));
        CheckBox checkBox3 = ((CheckBox)findViewById(R.id.checkBox3));
        Button button = ((Button)findViewById(R.id.button));

        SharedPreferences settings = getSharedPreferences("UserInfo", 0);


        // myVibrator.hasVibrator()
        checkbox1.setBackgroundColor(Color.rgb(0,158,115));
        checkbox1.setChecked(settings.getBoolean("checkbox1", false));

        // http://bconnelly.net/2013/10/creating-colorblind-friendly-figures/
        checkbox2.setBackgroundColor(Color.rgb(240,228,66));
        checkbox2.setChecked(settings.getBoolean("checkbox2", false));

        button.setBackgroundColor(Color.rgb(86,180,233));

        initMorse();
        myVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);


        checkBox3.setChecked(settings.getBoolean("checkbox3", false));
        if(this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)){
            checkBox3.setBackgroundColor(Color.rgb(213,94,0));

        } else {
            checkBox3.setChecked(false);
            checkBox3.setEnabled(false);

        }
    }

    private List createPattern(int hour, int min){
        List<long[]> output = new ArrayList();


        /* TODO: korjaa tämä
        *  getMorseNumber palauttaa morse patternin 0-9 numeron välillä
        *  jos numero on 15 pitäisi hakea 1 ja 5 patternit ja jos numero on 53
        *  pitäisi hakea numerot 5 ja 3
        */
        output.add(getMorseNumber(hour));
        output.add(getMorseNumber(min));

        return output;
    }

    public void RunStuff() throws CameraAccessException {
        DateTime dt = new DateTime();  // current time
        int minutes = dt.getMinuteOfDay();     // gets the current month
        int hours = dt.getHourOfDay(); // gets hour of day

        int dot = 200;
        int dash = 500;
        int short_gap = 200;
        int medium_gap = 500;
        int long_gap = 1000;

        List<long[]> pattern = createPattern(hours, minutes);




        if(((CheckBox)findViewById(R.id.checkBox)).isChecked()) {
            Vibra(pattern);
        }

        if(((CheckBox)findViewById( R.id.checkBox2)).isChecked()){
            Peep(pattern);
        }

        if(((CheckBox)findViewById( R.id.checkBox3)).isChecked()) {
            Flash(pattern);
        }
    }




    public void Vibra(List<long[]> pattern) {

        myVibrator.vibrate(getMorseNumber(7), -1);

        // TODO: Looppaa listan patterneja ja tärisee patternin
        for (int i=0; i < pattern.size(); ++i) {
//            myVibrator.vibrate(pattern.get(i), -1);

        }

    }

    public void Peep(List<long[]> pattern) {
        //todo: piippaa patternin mukaan

        for (int i=0; i < pattern.size(); ++i) {
            for (int j=0; j < pattern.get(i).length; ++j) {

            }
        }

        Pip1();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Pip2();
                // Actions to do after 1 second
            }
        }, 1000);
    }

    public void Flash(List<long[]> pattern) throws CameraAccessException {
        //todo: välky patternin mukaan
        ledon();

        for (int i=0; i < pattern.size(); ++i) {
            for (int j=0; j < pattern.get(i).length; ++j) {

                // sleep

                //ledoff();
            }
        }
    }

    // Piippaukset. Toinen on lyhyt ja toinen pitkä piippaus
    private void Pip1(){
        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, ToneGenerator.MAX_VOLUME);
        toneG.startTone(ToneGenerator.TONE_PROP_BEEP, 100);
    }

    private void Pip2(){
        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, ToneGenerator.MAX_VOLUME);
        toneG.startTone(ToneGenerator.TONE_CDMA_PIP, 100);
    }

    // Flashin hallinta funktiont
    private CameraManager camManager;
    private String cameraId;

    private void ledon() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            camManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            cameraId = null; // Usually back camera is at 0 position.
            try {
                cameraId = camManager.getCameraIdList()[0];
                camManager.setTorchMode(cameraId, true);   //Turn ON
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private void ledoff() throws CameraAccessException {
        camManager.setTorchMode(cameraId, false);
    }

    /* MORSE
        Näihin pääsee käsiksi getMorseNumber(numero)
        ja funktio palauttaa annetun numeron patternin 0-9 välillä
     */
    public long[] getMorseNumber(Integer i){
        if(i < 0 || i > 9) {
            return new long[]{};
        }

        return morseNumbers.get(i);
    }

    private HashMap<Integer, long[]> morseNumbers;
    private void initMorse(){
        int dot = 200;
        int dash = 500;
        int short_gap = 200;
        int medium_gap = 500;
        int long_gap = 1000;

        morseNumbers= new HashMap<Integer, long[]>();

        morseNumbers.put(0, new long[]{
                dash, short_gap, dash, short_gap, dash, short_gap, dash, short_gap, dash, long_gap
        });

        morseNumbers.put(1,new long[]{
                        dot, short_gap, dash, short_gap, dash, short_gap, dash, short_gap, dash, long_gap
        });

        morseNumbers.put(2, new long[]{
                dot, short_gap, dot, short_gap, dash, short_gap, dash, short_gap, dash, long_gap
        });

        morseNumbers.put(3, new long[]{
                dot, short_gap, dot, short_gap, dot, short_gap, dash, short_gap, dash, long_gap
        });

        morseNumbers.put(4, new long[]{
                dot, short_gap, dot, short_gap, dot, short_gap, dot, short_gap, dash, long_gap
        });

        morseNumbers.put(5, new long[]{
                dot, short_gap, dot, short_gap, dot, short_gap, dot, short_gap, dot, long_gap
        });

        morseNumbers.put(6, new long[]{
                dash, short_gap, dot, short_gap, dot, short_gap, dot, short_gap, dot, long_gap
        });

        morseNumbers.put(7, new long[]{
                dash, short_gap, dash, short_gap, dot, short_gap, dot, short_gap, dot, long_gap
        });

        morseNumbers.put(8, new long[]{
                dash, short_gap, dash, short_gap, dash, short_gap, dot, short_gap, dot, long_gap
        });

        morseNumbers.put(9, new long[]{
                dash, short_gap, dash, short_gap, dash, short_gap, dash, short_gap, dot, long_gap
        });
    }
}
